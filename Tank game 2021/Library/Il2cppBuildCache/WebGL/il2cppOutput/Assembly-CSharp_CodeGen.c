﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Controller::Start()
extern void Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157 (void);
// 0x00000002 System.Void Controller::Update()
extern void Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0 (void);
// 0x00000003 System.Void Controller::.ctor()
extern void Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7 (void);
// 0x00000004 System.Void MouseLooker::Start()
extern void MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437 (void);
// 0x00000005 System.Void MouseLooker::Update()
extern void MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55 (void);
// 0x00000006 System.Void MouseLooker::LockCursor(System.Boolean)
extern void MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70 (void);
// 0x00000007 System.Void MouseLooker::LookRotation()
extern void MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B (void);
// 0x00000008 UnityEngine.Quaternion MouseLooker::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56 (void);
// 0x00000009 System.Void MouseLooker::.ctor()
extern void MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98 (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157,
	Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0,
	Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7,
	MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437,
	MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55,
	MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70,
	MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B,
	MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56,
	MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98,
};
static const int32_t s_InvokerIndices[9] = 
{
	891,
	891,
	891,
	891,
	891,
	779,
	891,
	626,
	891,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	9,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
